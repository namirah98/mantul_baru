<!DOCTYPE html>
<html lang=en>
<head>
<title>Mantul Trader</title>
<meta charset=utf-8>
<meta name=viewport content="width=device-width,initial-scale=1,shrink-to-fit=no">
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300, 400,700" rel=stylesheet>
<link rel=stylesheet href="{{ asset('css/bootstrap.css') }}">
<link rel=stylesheet href=css/animate.css>
<link rel=stylesheet href=css/owl.carousel.min.css>
<link rel=stylesheet href=fonts/ionicons/css/ionicons.min.css>
<link rel=stylesheet href=fonts/fontawesome/css/font-awesome.min.css>
<link rel=stylesheet href=fonts/flaticon/font/flaticon.css>
<link rel=stylesheet href=https://use.fontawesome.com/releases/v5.7.2/css/all.css>
<link rel=stylesheet href=https://use.fontawesome.com/releases/v5.7.2/css/v4-shims.css>
<link href=https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css rel=stylesheet>
<link href=https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.8/css/mdb.min.css rel=stylesheet>
<link rel=stylesheet href=css/style.css>
</head>
<body>
<header role=banner>
<div class=top-bar>
<div class=container>
<div class=row>
<div class="col-9 social">
<nav class=nav>
<a class="nav-link active ijomen" href=/ >HOME</a>
<a class="nav-link ijomen" href=/method>METHOD</a>
<a class="nav-link ijomen" href=/home>ARTIKEL</a>
<a class="nav-link ijomen" href=/trend>TREND</a>
</nav>
</div>
<div class="col-3 social" style=text-align:right!important>
<a href=#><span class="fa fa-twitter"></span></a>
<a href=#><span class="fa fa-facebook"></span></a>
<a href=#><span class="fa fa-instagram"></span></a>
<a href=#><span class="fa fa-youtube-play"></span></a>
</div>
</div>
</div>
</div>
<div class="container logo-wrap">
<div class="row pt-5">
<div class="col-12 text-center">
<a class="absolute-toggle d-block d-md-none" data-toggle=collapse href=#navbarMenu role=button aria-expanded=false aria-controls=navbarMenu><span class=burger-lines></span></a>
<h5 class=site-logo>
<a href=/home>
<img src=images/mantulbarulogo.png alt="Image placeholder" class=img-fluid>
</a> <a href=/list/buying-low-selling-high-6>Buying Low Selling High</a>
</h5>
</div>
</div>
</div>
<nav class="navbar navbar-expand-md navbar-light bg-light">
</nav>
</header>
@yield("content")
<footer class=site-footer>
<footer class="page-footer font-small">
<div class=container>
<div class="row text-center d-flex justify-content-center pt-5 mb-3">
<div class="col-md-2 mb-3">
<h6 class="text-uppercase font-weight-bold">
<a href=/aboutus>About us</a>
</h6>
</div>
<div class="col-md-2 mb-3">
<h6 class="text-uppercase font-weight-bold">
<a href=#!>Risk Disclosure</a>
</h6>
</div>
<div class="col-md-2 mb-3">
<h6 class="text-uppercase font-weight-bold">
<a href=#!>Awards</a>
</h6>
</div>
<div class="col-md-2 mb-3">
<h6 class="text-uppercase font-weight-bold">
<a href=#!>Help</a>
</h6>
</div>
<div class="col-md-2 mb-3">
<h6 class="text-uppercase font-weight-bold">
<a href=#!>Contact</a>
</h6>
</div>
</div>
<hr class=rgba-white-light style="margin:0 15%">
<div class="row d-flex text-center justify-content-center mb-md-0 mb-4">
<div class="col-md-8 col-12 mt-5">
<p style=line-height:1.7rem>
<span class="sflogo h3">
<img class="img img-fluid sf-pad-top-20" src=images/footer-disclaimer.png width=80px height=82px alt=Disclaimer>CLAIMER :
</span>
<ul class="text-def sf-penulis text-def sf-disclaim-footer">
<li>Trading market adalah berisiko tinggi. Tanggung jawab atas hasil dari segala keputusan ada pada Anda sendiri.</li>
<li>Informasi disajikan sebaik mungkin, tetapi kami tidak menjamin 100% akurasi semua rekomendasi yang dihadirkan.</li>
<li>Kami tidak menjamin kualitas materi promosi yang disediakan pihak ketiga berupa iklan berbayar, banner, dll.</li>
<li>Materi dihadirkan untuk tujuan edukasi dan tidak diperkenankan untuk menyalin.</li>
<li>Ikuti <a href=/advertising/terms_of_use.php>Terms of Use</a> untuk menggunakan materi sebagai referensi.</li>
</ul>
</p>
</div>
</div>
<hr class="clearfix d-md-none rgba-white-light" style="margin:10% 15% 5%">
<div class="row pb-3">
<div class=col-md-12>
<div class="mb-5 flex-center">
<a class=fb-ic>
<i class="fab fa-facebook-f fa-lg white-text mr-4"> </i>
</a>
<a class=tw-ic>
<i class="fab fa-twitter fa-lg white-text mr-4"> </i>
</a>
<a class=gplus-ic>
<i class="fab fa-google-plus-g fa-lg white-text mr-4"> </i>
</a>
<a class=li-ic>
</a>
<a class=ins-ic>
<i class="fab fa-instagram fa-lg white-text mr-4"> </i>
</a>
<a class=pin-ic>
</a>
</div>
</div>
</div>
</div>
<div class="footer-copyright text-center py-3" style=color:#f0f8ff>
Copyright &copy;<script>document.write((new Date).getFullYear())</script> mantultrader.com
</div>
</footer>
<div class=row>
<div class=col-md-12 style=color:#f0f8ff>
</div>
</div>

</footer>
<div id=loader class="show fullscreen"><svg class=circular width=48px height=48px><circle class=path-bg cx=24 cy=24 r=22 fill=none stroke-width=4 stroke=#eeeeee /><circle class=path cx=24 cy=24 r=22 fill=none stroke-width=4 stroke-miterlimit=10 stroke=#f4b214 /></svg></div>
<script src=js/jquery-3.2.1.min.js></script>
<script src=js/jquery-migrate-3.0.0.js></script>
<script src=js/popper.min.js></script>
<script src=js/bootstrap.min.js></script>
<script src=js/owl.carousel.min.js></script>
<script src=js/jquery.waypoints.min.js></script>
<script src=js/jquery.stellar.min.js></script>
<script src=https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js></script>
<script src=https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js></script>
<script src=https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js></script>
<script src=https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.8/js/mdb.min.js></script>
<script src=js/main.js></script>
</body>
</html>
