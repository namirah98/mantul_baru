@extends('layout')
@section("content")
<section class=" -section pt-5">
<div class="container">

<!-- Jumbotron -->
<div class="jumbotron text-center">

  <!-- Title -->
  <h4 class="card-title h4 pb-2"><strong>Mantul Method</strong></h4>

  <!-- Card image -->
  <div class="view overlay my-4" >
    <img   src="images/Capture.PNG" width="40%" height="420%" style="display: block; margin: auto; class="img-fluid" alt="">
    <a href="#">
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>

  <h5 class="indigo-text h5 mb-4">Apa Yang Harus Kita Lakukan?</h5>

  <p class="card-text">Let's See</p>

<!-- Section: Blog v.1 -->
<section class="my-5">

  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center my-5">MantulTrader</h2>
  <!-- Section description -->
  <p class="text-center w-responsive mx-auto mb-5">Mantul Trader merupakan salah satu taktik untuk melakukan trading dengan hukum dasar perdagangan.
  Yaitu membeli harga diharga rendah dan menjualnya diharga tinggi. Berikut cara MantulTrader melakukannya.  </p>



  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
        <img class="img-fluid" src="images/Capture.PNG" alt="Sample image">
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Category -->

      <a href="#!" class="green-text">
        <h6 class="font-weight-bold mb-3"><!--<i class="fas fa-utensils pr-2"></i>-->1</h6>
      </a>
      <!-- Post title -->
      <h3 class="font-weight-bold mb-3"><strong>Mengindentifikasi Trend </strong></h3>
      <h4>Trend is your friend</h4>

      <!-- Excerpt -->
      <p>Trend is your friend. Yap trend adalah teman kita, kenapa? jika Sobat ingin berpeluang untung lebih besar memiliki peluang untuk menang,kita harus mengikuti trend yang ada.
      Langkah pertama adalah kita harus tahu kondisi trend yang sedang terjadi pada market. Apakah trend  sedang naik atau trend sedang turun? langkah ini cukup penting untuk diperhatikan,
      kita bisa mengetahui kapan kita bisa memulai untuk masuk dan kapan kita akan keluar.</p>
       <a class="btn btn-success btn-md" href="http://mantultrader.com/list/mengidentifikasi-trend-dengan-jurus-mantul-11">Read more</a>
      <!-- Post data -->

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-5">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Category -->
      <a href="#!" class="pink-text">
        <h6 class="font-weight-bold mb-3">2</h6>
      </a>
      <!-- Post title -->
      <h3 class="font-weight-bold mb-3"><strong>Cara Melihat Trend</strong></h3>
      <!-- Excerpt -->
      <p>Langkah berikutnya adalah mengetahui bentuk dari pola  trend yang sedang naik atau trend yang sedang turun.
      Jika grafik higher high dan higher low maka dia mengalami trend naik. Untuk kondisi trend turun sendiri yaitu lower low dan lower high.  </p>
       <a class="btn btn-success btn-md" href="http://mantultrader.com/list/cara-mengetahui-ciri-dan-kondisi-trend-12">Read more</a>
      <!-- Post data -->


    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-5">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-2">
        <img class="img-fluid" src="images/f.png" alt="Sample image">
        <p>Contoh trend naik.</p>


        <hr>
        <img class="img-fluid" src="images/f2.png" alt="Sample image">
        <p>Contoh trend turun.</p>

        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-5">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
        <img class="img-fluid" src="images/1.png" alt="Sample image">
         <p>Pola Trend Naik</p>


        <hr>
         <img class="img-fluid" src="images/2.png" alt="Sample image">
         <p>Pola Trend Turun</p>
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Category -->
      <a href="#!" class="indigo-text">
        <h6 class="font-weight-bold mb-3">3</h6>
      </a>
      <!-- Post title -->
      <h3 class="font-weight-bold mb-3"><strong>Adanya Pola Mantul</strong></h3>
      <h4>Buying low selling high . </h4>
      <!-- Excerpt -->
      <p>Langkah ke-3 adalah adanya pola mantul pada trend. Yaitu  melihat pola yang ada, jika pola tersebut menyerupai huruf 'W' maka trend sedang naik.
      Jika pola mantul berbentuk menyerupai huruf 'M' maka trend sedang turun. Kita bisa masuk disaat salah satu kondisi terjadi. </p>
       <a class="btn btn-success btn-md" href="http://mantultrader.com/news/melihat-pola-mantul-dengan-jurus-mantul-5">Read more</a>
      <!-- Post data -->
    </div>
    <!-- Grid column -->


  </div>
  <!-- Grid row -->

</section>
<!-- Section: Blog v.1 -->


</div>
</section>
@endsection
