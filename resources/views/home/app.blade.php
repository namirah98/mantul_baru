<!doctype html>
<html lang="en">
  <head>
    <title>Mantul Trader</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300, 400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>



    <header role="banner">
      <div class="top-bar">
        <div class="container">
          <div class="row">
            <div class="col-9 social">
                <nav class="nav">
                  <a class="nav-link active" href="/home">HOME</a>
                  <a class="nav-link" href="/blog">METHOD</a>
                  <a class="nav-link" href="/list">ARTIKEL</a>
                  <a class="nav-link" href="/polls">TREND</a>
                </nav>
            </div>

            <div class="col-3 social" style="text-align: right !important">
            <a href="#"><span class="fa fa-twitter"></span></a>
              <a href="#"><span class="fa fa-facebook"></span></a>
              <a href="#"><span class="fa fa-instagram"></span></a>
              <a href="#"><span class="fa fa-youtube-play"></span></a>
            </div>
          </div>
        </div>
      </div>

      <div class="container logo-wrap">
        <div class="row pt-5">
          <div class="col-12 text-center">
            <a class="absolute-toggle d-block d-md-none" data-toggle="collapse" href="#navbarMenu" role="button" aria-expanded="false" aria-controls="navbarMenu"><span class="burger-lines"></span></a>

            <h5 class="site-logo">
                <a href="index.html">
                    <img src="images/mantulbarulogo.png" alt="Image placeholder" class="img-fluid">
                    Buying Low Selling High</a>
            </h5>
          </div>
        </div>
      </div>

      <nav class="navbar navbar-expand-md  navbar-light bg-light">
        <div class="container">


          <div class="collapse navbar-collapse" id="navbarMenu">
            <ul class="navbar-nav mx-auto">

            </ul>

          </div>
        </div
      </nav>
    </header>
    <!-- END header -->

    @yield("content")

    <footer class="site-footer">
      <div class="container">
        <div class="row mb-5">
         <!-- <div class="col-md-4">
            <h3>MantulTrader</h3>
            <p>
              <img src="images/mantulbarulogo.png" alt="Image placeholder" class="img-fluid">
            </p>

            <p style="color: aliceblue !important"><i class="fa fa-map-marker icon-white"></i> Palma One Building, 7th Floor Jl. H. R. Rasuna Said, Kuningan Jakarta Selatan, 12950.</p>
            <p style="color: aliceblue !important"><i class="fa fa-envelope icon-white"></i> info@mantultrader.com</p>
            <p style="color: aliceblue !important"><i class="fa fa-phone icon-white"></i> 021-5225631</p>

          </div>-->
          <div class="col-md-6 ml-auto">
            <div class="row">
              <div class="col-md-7">
                <h3></h3>
                <div class="post-entry-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="list-unstyled">
                    <li><a href="/aboutus">About Us</a></li>
                    <li><a href="#">Risk Disclosure</a></li>

                  </ul>

                </div>
              </div>
              <div class="col-md-1"></div>

              <div class="col-md-4">

                <div class="mb-5">
                 <!-- <h3>Quick Links</h3>
                  <ul class="list-unstyled">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Travel</a></li>
                    <li><a href="#">Adventure</a></li>
                    <li><a href="#">Courses</a></li>
                    <li><a href="#">Categories</a></li>
                  </ul>-->
                  <h3>Social</h3>
                  <ul class="list-unstyled footer-social">
                    <li><a href="#"><span class="fa fa-twitter"></span> Twitter</a></li>
                    <li><a href="#"><span class="fa fa-facebook"></span> Facebook</a></li>
                    <li><a href="#"><span class="fa fa-instagram"></span> Instagram</a></li>
                    <li><a href="#"><span class="fa fa-youtube-play"></span> Youtube</a></li>
                  </ul>
                </div>

                <div class="post-entry-sidebar">

                </div>
              </div>
            </div>
          </div>
        </div>
       <!-- <div class="row">
        <div class="col-md-12">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2358.3592326554003!2d106.83183223500662!3d-6.227871594272405!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3ee2343a1b1%3A0x44eddf7e54d4fdaf!2sPalma+One%2C+Jl.+H.+R.+Rasuna+Said%2C+Kuningan+Tim.%2C+Kecamatan+Setiabudi%2C+Kota+Jakarta+Selatan%2C+Daerah+Khusus+Ibukota+Jakarta+12950!5e0!3m2!1sen!2sid!4v1526540921585" width="100%" height="355" frameborder="0" style="border:0"></iframe>
        </div>-->
        <div class="row">
          <div class="col-md-12" style="color: aliceblue" >
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> mantultrader.com
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </div>
        </div>
      </div>
    </footer>
    <!-- END footer -->

    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/jquery-migrate-3.0.0.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>


    <script src="js/main.js"></script>
  </body>
</html>
