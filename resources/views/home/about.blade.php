@extends('layout')
@section("content")
<section class="site-section pt-5">
<!-- Card deck -->
<div class="container-fluid mt-2">
<div class="card-deck">

  <!-- Card -->
  <div class="card mb-4" style="border-radius: 10px">

    <!--Card image-->
    <div class="view overlay" style="border-top-left-radius: 10px;border-top-right-radius: 10px">
      <img class="card-img-top img-fluid" style="border-top-left-radius: 10px;border-top-right-radius: 10px" src="images/location_map_pin_dark_green6.png" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!--Card content-->
    <div class="card-body" >

      <!--Title-->
      <h4 class="card-title">Palma One Building, 7th Floor Jl. H. R. Rasuna Said, Kuningan Jakarta Selatan, 12950.</h4>
      <!--Text-->
      <p class="card-text">Look at this map below.</p>
      <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
      <!--<button type="button" class="btn btn-light-blue btn-md">Read more</button>-->

    </div>

  </div>
  <!-- Card -->

  <!-- Card -->
  <div class="card mb-4" style="border-radius: 10px">

    <!--Card image-->
    <div class="view overlay"  style="border-top-left-radius: 10px;border-top-right-radius: 10px">
      <img class="card-img-top img-fluid" src="images/images.png" alt="Card image cap"  style="border-top-left-radius: 10px;border-top-right-radius: 10px">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!--Card content-->
    <div class="card-body">

      <!--Title-->
      <h4 class="card-title">info@mantultrader.com</h4>
      <!--Text-->
      <!--<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
      <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
      <!--<button type="button" class="btn btn-light-blue btn-md">Read more</button>-->

    </div>

  </div>
  <!-- Card -->

  <!-- Card -->
  <div class="card mb-4" style="border-radius: 10px">

    <!--Card image-->
    <div class="view overlay"  style="border-top-left-radius: 10px;border-top-right-radius: 10px">
      <img class="card-img-top img-fluid" src="images/phone-28-xxl.png" alt="Card image cap"  style="border-top-left-radius: 10px;border-top-right-radius: 10px">

      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!--Card content-->
    <div class="card-body">

      <!--Title-->
      <h4 class="card-title"> 021-5225631</h4>
      <!--Text-->
      <p class="card-text">Business hours</p>
      <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
      <!--<button type="button" class="btn btn-light-blue btn-md">Read more</button>-->

    </div>

  </div>
  <!-- Card -->

</div>
<!-- Card deck -->
 <div class="row">
        <div class="col-md-12">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2358.3592326554003!2d106.83183223500662!3d-6.227871594272405!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3ee2343a1b1%3A0x44eddf7e54d4fdaf!2sPalma+One%2C+Jl.+H.+R.+Rasuna+Said%2C+Kuningan+Tim.%2C+Kecamatan+Setiabudi%2C+Kota+Jakarta+Selatan%2C+Daerah+Khusus+Ibukota+Jakarta+12950!5e0!3m2!1sen!2sid!4v1526540921585" width="100%" height="355" frameborder="0" style="border:0"></iframe>
        </div>
        </div>
</section>
@endsection
