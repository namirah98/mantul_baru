@extends('layout') @section("content")
<section class="site-section pt-5">
    <div class=container>
        <div class=row>
            <div class=col-md-12>
                <div class="owl-carousel owl-theme home-slider">
                    <div>
                        <a href=blog-single.html class="a-block d-flex align-items-center height-lg" style=background-image:url(images/img_1.jpg)>
                        </a>
                    </div>
                    <div>
                        <a href=blog-single.html class="a-block d-flex align-items-center height-lg" style=background-image:url(images/img_2.jpg)>
                        </a>
                    </div>
                    <div>
                        <a href=blog-single.html class="a-block d-flex align-items-center height-lg" style=background-image:url(images/img_3.jpg)>
                        </a>
                    </div>
                    <div>
                        <a href=blog-single.html class="a-block d-flex align-items-center height-lg" style=background-image:url(images/img_4.jpg)>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class=container>
            <div class=video-text>
                <h2>Our Method</h2>
            </div>
        </div>
        <div class=row>
            <div class="col-md-6 col-lg-4">
                <a href=/news/test-1-buat-simpel-method-8 class="a-block d-flex align-items-center height-md" style=background-image:url(images/TEST.png)>
                    <div class=text>
                        <div class=post-meta>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4">
                <a href=/news/test-2-buat-greate-return-9 class="a-block d-flex align-items-center height-md" style=background-image:url(images/TEST2.png)>
                    <div class=text>
                        <div class=post-meta>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4">
                <a href=/news/test-3-buat-limited-risk-10 class="a-block d-flex align-items-center height-md" style=background-image:url(images/k3.png)>
                    <div class=text>
                        <div class=post-meta>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="site-section py-sm">
    <div class=container>
        <div class=row>
            <div class=col-md-6>
                <div class=video-text>
                    <h2>Introduction Video</h2>
                    <p>Watch the video!</p>
                    <p></p>
                </div>
                <section class=# data-setbg=#>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class=embed-responsive-item src=https://www.youtube.com/embed/F8sTh_oc2RU frameborder=0 allowfullscreen></iframe>
                    </div>
                    <br>
                </section>
            </div>
            <div class=col-md-6>
                <div class=video-text>
                    <h2>How we do with our method?</h2>
                    <p>Click and Happy Trading With Our Method.</p>
                    <p></p>
                </div>
                <div class=card>
                    <img class=card-img-top src=images/css-animation-craziness.gif alt="Card image cap">
                    <div class=card-body>
                        <h4 class=card-title><a>Welcome to MantulTrader</a></h4>
                        <p class=card-text>Mantultrader.com helps individual traders learn how to be a good trader in the market. We're also a community of traders that support each other on our daily trading journey.
                        </p>
                        <a href=/home>
                            <button type=button class="btn btn-success">NEXT</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt-5">
        <div class=card-group>
            <div class="card mb-4 mr-3" style=border-radius:10px>
                <div class="view overlay" style=border-top-left-radius:10px;border-top-right-radius:10px>
                    <img class=card-img-top style=border-top-left-radius:10px;border-top-right-radius:10px src="http://mantultrader.com/upload/media/posts/{{$blog[0]['thumb']}}-s.jpg" alt="{{$blog[0]['title']}}">
                    <a href=#!>
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>
                <div class=card-body>
                    <h4 class=card-title>{{$blog[0]['title']}}</h4>
                    <p class=card-text>{{str_limit($blog[0]['body'] , 85)}}</p>
                    <div class=float-right><a href="{{url($blog[0]['type'].'/'.$blog[0]['slug'].'-'.$blog[0]['id'])}}" target=_blank class="font-biru hvr-forward">Selengkapnya</a></div>
                </div>
            </div>
            <div class="card mb-4 mr-3" style=border-radius:10px>
                <div class="view overlay" style=border-top-left-radius:10px;border-top-right-radius:10px>
                    <img class=card-img-top style=border-top-left-radius:10px;border-top-right-radius:10px src="http://mantultrader.com/upload/media/posts/{{$blog[1]['thumb']}}-s.jpg" alt="{{$blog[0]['title']}}">
                    <a href=#!>
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>
                <div class=card-body>
                    <h4 class=card-title>{{$blog[1]['title']}}</h4>
                    <p class=card-text>{{str_limit($blog[1]['body'] , 85)}}</p>
                    <div class=float-right><a href="{{url($blog[1]['type'].'/'.$blog[1]['slug'].'-'.$blog[1]['id'])}}" target=_blank class="font-biru hvr-forward">Selengkapnya</a></div>
                </div>
            </div>
            <div class="card mb-4" style=border-radius:10px>
                <div class="view overlay" style=border-top-left-radius:10px;border-top-right-radius:10px>
                    <img class=card-img-top style=border-top-left-radius:10px;border-top-right-radius:10px src="http://mantultrader.com/upload/media/posts/{{$blog[2]['thumb']}}-s.jpg" alt="{{$blog[0]['title']}}">
                    <a href=#!>
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>
                <div class=card-body>
                    <h4 class=card-title>{{$blog[2]['title']}}</h4>
                    <p class=card-text>{{str_limit($blog[2]['body'] , 85)}}</p>
                    <div class=float-right><a href="{{url($blog[2]['type'].'/'.$blog[2]['slug'].'-'.$blog[2]['id'])}}" target=_blank class="font-biru hvr-forward">Selengkapnya</a></div>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection
