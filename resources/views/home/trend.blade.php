@extends('layout')
@section("content")
<section class="site-section pt-5">
   <!-- Section heading -->

<!-- Section description -->

<!-- Grid row -->
<div class="row d-flex justify-content-center">

  <!-- Grid column -->
  <div class="col-md-6 col-xl-5 mb-4">
    <!--Featured image-->
    <div class="view overlay rounded z-depth-2">
      <iframe src="https://www.widgets.investing.com/live-currency-cross-rates?theme=darkTheme&pairs=1,3,2,4,7,5,8,6,9,10,49,11" width="100%" height="600" frameborder="0" allowtransparency="true" marginwidth="0" marginheight="0"></iframe><div class="poweredBy" style="font-family: Arial, Helvetica, sans-serif;">Powered by <a href="https://www.investing.com?utm_source=WMT&amp;utm_medium=referral&amp;utm_campaign=LIVE_CURRENCY_X_RATES&amp;utm_content=Footer%20Link" target="_blank" rel="nofollow">Investing.com</a></div>
    </div>
    <!--Excerpt-->
    <div class="card-body">
      
   
      
    </div>
  </div>
  <!-- Grid column -->

  <!-- Grid column -->
  <div class="col-md-6 col-xl-5 mb-4">
    <!--Featured image-->
    <div class="view overlay rounded z-depth-2">
   <iframe src="https://www.widgets.investing.com/live-indices?theme=darkTheme&pairs=166,27,172,177,170" width="100%" height="600" frameborder="0" allowtransparency="true" marginwidth="0" marginheight="0"></iframe><div class="poweredBy" style="font-family: Arial, Helvetica, sans-serif;">Powered by <a href="https://www.investing.com?utm_source=WMT&amp;utm_medium=referral&amp;utm_campaign=LIVE_INDICES&amp;utm_content=Footer%20Link" target="_blank" rel="nofollow">Investing.com</a></div>
    </div>
    <!--Excerpt-->
    <div class="card-body">
      
    </div>
  </div>
  <!-- Grid column -->

</div>
<!-- Grid row -->

<!-- Grid row -->

<!-- Grid row -->
<div class="card">

  <!--Card image-->
<iframe src="https://sslecal2.forexprostools.com?columns=exc_flags,exc_currency,exc_importance,exc_actual,exc_forecast,exc_previous&features=datepicker,timezone&countries=25,32,6,37,72,22,17,39,14,10,35,43,56,36,110,11,26,12,4,5&calType=week&timeZone=8&lang=1" width="650" height="467" frameborder="0" allowtransparency="true" marginwidth="0" marginheight="0"></iframe><div class="poweredBy" style="font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 11px;color: #333333;text-decoration: none;">Real Time Economic Calendar provided by <a href="https://www.investing.com/" rel="nofollow" target="_blank" style="font-size: 11px;color: #06529D; font-weight: bold;" class="underline_link">Investing.com</a>.</span></div>

  <!--Card content-->
  <div class="card-body">

    <!-- Container -->
    <div class="d-block d-md-flex">

      <!-- Column -->
      <div class="p-3 flex-1">

       

      </div>
      <!-- Column -->

      <!-- Column -->
      <div class="p-3 flex-1">

        

      </div>
      <!-- Column -->

      <!-- Column -->
      <div class="p-3 flex-1">
<div class="card">
        <iframe height="480" width="650" src="https://ssltvc.forexprostools.com/?pair_ID=8849&height=480&width=650&interval=300&plotStyle=area&domain_ID=1&lang_ID=1&timezone_ID=21"></iframe>
  </div>
      </div>
      <!-- Column -->

    </div>
    <!-- Container -->

  </div>
</div>
</section>
@endsection
