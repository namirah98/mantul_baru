@unless(count($lastFeaturestop)==0)
    <div class="headline-articles headline-slider headline-articles--wide">
        <div class="headline-articles__container headline-slider__container">
            @foreach($lastFeaturestop as $item)
                <div class="headline-articles__box headline-articles__box--news headline-slider__box">
                    <a href="{{ makeposturl($item) }}" title="{{ $item->title }}" >
                        <figure class="headline-articles__image">
                            <img src="{{ makepreview($item->thumb, 'b', 'posts') }}" alt="{{ $item->title }}" width="525" height="295">
                        </figure>
                        <div class="headline-articles__content">
                            <div class="headline-articles__content__row">
                                <div class="headline-articles__content__time" style="margin:0">
                                    <time  style="padding:0" datetime="{{ $item->created_at->toW3cString() }}">{{ $item->created_at->diffForHumans() }}</time>
                                </div>
                            </div>
                            <h3 class="headline-articles__content__title">
                                {{ $item->title }}
                            </h3>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        <i class="headline-articles__navigation headline-articles__navigation--prev headline-slider__navigation headline-slider__navigation--prev material-icons">&#xE314;</i>
        <i class="headline-articles__navigation headline-articles__navigation--next headline-slider__navigation headline-slider__navigation--next material-icons">&#xE315;</i>
    </div>
@endunless
<!--
@unless(count($lastFeaturestop)==0)

<section class="headline hide-phone">
<div class="global-container container" style="padding-top:0 !important;">
        @foreach($lastFeaturestop->slice(0,3) as $key => $item)
        @if($key == 0)
        <div class="slider" id="headline-slider" data-pagination="true" data-navigation="false">
                <div class="slider__list">
                        <article class="slider__item headline__blocks headline__blocks">
                            <div class="headline__blocks__image" style="background-image: url({{ makepreview($item->thumb, 'b', 'posts') }})"></div>
                            <a class="headline__blocks__link" href="{{ makeposturl($item) }}" title="{{ $item->title }}" ></a>
                            <header class="headline__blocks__header">
                                <h2 class="headline__blocks__header__title headline__blocks__header__title">{{ $item->title }}</h2>
                                <ul class="headline__blocks__header__other">
                                    <li class="headline__blocks__header__other__author">{{ $item->user->username }}</li>
                                    <li class="headline__blocks__header__other__date"><i class="material-icons">&#xE192;</i> <time datetime="{{ $item->created_at->toW3cString() }}">{{ $item->created_at->diffForHumans() }}</time></li>
                                </ul>
                            </header>
                        </article>
                </div>
            </div>
        @else
        <div style="margin-left:-4px; margin-right:-3px;">
        <article class="headline__blocks headline__blocks--small " style=" width: 34%;    height: 237px; ">
            <div class="headline__blocks__image" style="background-image: url({{ makepreview($item->thumb, 'b', 'posts') }})"></div>
            <a class="headline__blocks__link" href="{{ makeposturl($item) }}" title="{{ $item->title }}" ></a>
            <header class="headline__blocks__header">
                <h2 class="headline__blocks__header__title headline__blocks__header__title--small ">{{ $item->title }}</h2>
                <ul class="headline__blocks__header__other">
                    <li class="headline__blocks__header__other__author">{{ $item->user->username }}</li>
                    <li class="headline__blocks__header__other__date"><i class="material-icons"></i> <time datetime="{{ $item->created_at->toW3cString() }}">{{ $item->created_at->diffForHumans() }}</time></li>
                </ul>
            </header>
        </article>
         <div class="clear"></div>
        </div>
        @endif
        @endforeach
   </div>
</section>
<section class="headline visible-phone">
    <div class="slider" id="headline-slider" data-pagination="true" data-navigation="false">
        <div class="slider__list">
            @foreach($lastFeaturestop->slice(0,4) as $key => $item)
                <article class="slider__item headline__blocks headline__blocks--phone">
                    <div class="headline__blocks__image" style="background-image: url({{ makepreview($item->thumb, 'b', 'posts') }})"></div>
                    <a class="headline__blocks__link" href="{{ makeposturl($item) }}" title="{{ $item->title }}" ></a>
                    <header class="headline__blocks__header">
                        <h2 class="headline__blocks__header__title headline__blocks__header__title--phone">{{ $item->title }}</h2>
                        <ul class="headline__blocks__header__other">
                            <li class="headline__blocks__header__other__author">{{ $item->user->username }}</li>
                            <li class="headline__blocks__header__other__date"><i class="material-icons">&#xE192;</i> <time datetime="{{ $item->created_at->toAtomString() }}">{{ $item->created_at->diffForHumans() }}</time></li>
                        </ul>
                    </header>
                </article>
            @endforeach
        </div>
    </div>
</section>
@endunless -->
