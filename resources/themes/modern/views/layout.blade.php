<!doctype html>
<html lang="en">
  <head>
    <title>Mantul Trader</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300, 400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/v4-shims.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.8/css/mdb.min.css" rel="stylesheet">

    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>



    <header role="banner">
      <div class="top-bar">
        <div class="container">
          <div class="row">
            <div class="col-9 social">
                <nav class="nav">
                  <a class="nav-link active ijomen" href="/">HOME</a>
                  <a class="nav-link ijomen" href="/method">METHOD</a>
                  <a class="nav-link ijomen" href="/home">ARTIKEL</a>
                  <a class="nav-link ijomen" href="/trend">TREND</a>
                </nav>
            </div>

            <div class="col-3 social" style="text-align: right !important">
            <a href="#"><span class="fa fa-twitter"></span></a>
              <a href="#"><span class="fa fa-facebook"></span></a>
              <a href="#"><span class="fa fa-instagram"></span></a>
              <a href="#"><span class="fa fa-youtube-play"></span></a>
            </div>
          </div>
        </div>
      </div>

      <div class="container logo-wrap">
        <div class="row pt-5">
          <div class="col-12 text-center">
            <a class="absolute-toggle d-block d-md-none" data-toggle="collapse" href="#navbarMenu" role="button" aria-expanded="false" aria-controls="navbarMenu"><span class="burger-lines"></span></a>

            <h5 class="site-logo">
                <a href="/home">
                    <img src="images/mantulbarulogo.png" alt="Image placeholder" class="img-fluid">
                    </a> <a href="/list/buying-low-selling-high-6">Buying Low Selling High</a>
            </h5>
          </div>
        </div>
      </div>

      <nav class="navbar navbar-expand-md  navbar-light bg-light">

      </nav>
    </header>
    <!-- END header -->

    @yield("content")

    <footer class="site-footer">
 <!--     <div class="container">
        <div class="row mb-5">
          <!--<div class="col-md-4">
            <h3>MantulTrader</h3>
            <p>
              <img src="images/mantulbarulogo.png" alt="Image placeholder" class="img-fluid">
            </p>

            <p style="color: aliceblue !important"><i class="fa fa-map-marker icon-white"></i> Palma One Building, 7th Floor Jl. H. R. Rasuna Said, Kuningan Jakarta Selatan, 12950.</p>
            <p style="color: aliceblue !important"><i class="fa fa-envelope icon-white"></i> info@mantultrader.com</p>
            <p style="color: aliceblue !important"><i class="fa fa-phone icon-white"></i> 021-5225631</p>

          </div>
          <div class="col-md-6 ml-auto">
            <div class="row">
              <div class="col-md-7">
                <h3></h3>
                <div class="post-entry-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="list-unstyled">
                    <li><a href="/aboutus">About Us</a></li>
                    <li><a href="/risk">Risk Disclosure</a></li>

                  </ul>

                </div>
              </div>




                <div class="mb-5">
                 <!-- <h3>Quick Links</h3>
                  <ul class="list-unstyled">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Travel</a></li>
                    <li><a href="#">Adventure</a></li>
                    <li><a href="#">Courses</a></li>
                    <li><a href="#">Categories</a></li>
                  </ul>-->
      <!--            <h3>Social</h3>
                  <ul class="list-unstyled footer-social">
                    <li><a href="#"><span class="fa fa-twitter"></span> Twitter</a></li>
                    <li><a href="#"><span class="fa fa-facebook"></span> Facebook</a></li>
                    <li><a href="#"><span class="fa fa-instagram"></span> Instagram</a></li>
                    <li><a href="#"><span class="fa fa-youtube-play"></span> Youtube</a></li>
                  </ul>
                </div>



            </div>
          </div>
        </div>-->
        <!--<div class="row">
        <div class="col-md-12">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2358.3592326554003!2d106.83183223500662!3d-6.227871594272405!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3ee2343a1b1%3A0x44eddf7e54d4fdaf!2sPalma+One%2C+Jl.+H.+R.+Rasuna+Said%2C+Kuningan+Tim.%2C+Kecamatan+Setiabudi%2C+Kota+Jakarta+Selatan%2C+Daerah+Khusus+Ibukota+Jakarta+12950!5e0!3m2!1sen!2sid!4v1526540921585" width="100%" height="355" frameborder="0" style="border:0"></iframe>
        </div>-->
        <!-- Footer -->
<footer class="page-footer font-small">

  <!-- Footer Links -->
  <div class="container">

    <!-- Grid row-->
    <div class="row text-center d-flex justify-content-center pt-5 mb-3">

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a href="/aboutus">About us</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a href="#!">Risk Disclosure</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a href="#!">Awards</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a href="#!">Help</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a href="#!">Contact</a>
        </h6>
      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row-->
    <hr class="rgba-white-light" style="margin: 0 15%;">

    <!-- Grid row-->
    <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">

      <!-- Grid column -->
      <div class="col-md-8 col-12 mt-5">
        <p style="line-height: 1.7rem">

  <span class="sflogo h3">
      <img class="img img-fluid sf-pad-top-20" src="images/footer-disclaimer.png" width="80px" height="82px" alt="Disclaimer">CLAIMER :
    </span>
  <ul class="text-def sf-penulis text-def sf-disclaim-footer">
     <li>Trading market adalah berisiko tinggi. Tanggung jawab atas hasil dari segala keputusan ada pada Anda sendiri.</li>
     <li>Informasi disajikan sebaik mungkin, tetapi kami tidak menjamin 100% akurasi semua rekomendasi yang dihadirkan.</li>
     <li>Kami tidak menjamin kualitas materi promosi yang disediakan pihak ketiga berupa iklan berbayar, banner, dll.</li>
     <li>Materi dihadirkan untuk tujuan edukasi dan tidak diperkenankan untuk menyalin.</li>
     <li>Ikuti <a href="/advertising/terms_of_use.php">Terms of Use</a> untuk menggunakan materi sebagai referensi.</li>
   </ul>

        </p>
      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row-->
    <hr class="clearfix d-md-none rgba-white-light" style="margin: 10% 15% 5%;">

    <!-- Grid row-->
    <div class="row pb-3">

      <!-- Grid column -->
      <div class="col-md-12">

        <div class="mb-5 flex-center">

          <!-- Facebook -->
          <a class="fb-ic">
            <i class="fab fa-facebook-f fa-lg white-text mr-4"> </i>
          </a>
          <!-- Twitter -->
          <a class="tw-ic">
            <i class="fab fa-twitter fa-lg white-text mr-4"> </i>
          </a>
          <!-- Google +-->
          <a class="gplus-ic">
            <i class="fab fa-google-plus-g fa-lg white-text mr-4"> </i>
          </a>
          <!--Linkedin -->
          <a class="li-ic">
            <!--<i class="fab fa-linkedin-in fa-lg white-text mr-4"> </i>-->
          </a>
          <!--Instagram-->
          <a class="ins-ic">
            <i class="fab fa-instagram fa-lg white-text mr-4"> </i>
          </a>
          <!--Pinterest-->
          <a class="pin-ic">
           <!-- <i class="fab fa-pinterest fa-lg white-text"> </i>-->
          </a>

        </div>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row-->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3" style="color: aliceblue">
  <!--  <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>-->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> mantultrader.com
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
        <div class="row">
          <div class="col-md-12" style="color: aliceblue" >
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
               <!-- Copyright &copy;<script>document.write(new Date().getFullYear());</script> mantultrader.com-->
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </div>
        </div>
      </div>
    </footer>
    <!-- END footer -->

    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/jquery-migrate-3.0.0.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.8/js/mdb.min.js"></script>


    <script src="js/main.js"></script>
  </body>
</html>
